#ifndef MQ
#define MQ

#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <erebor_wrapper.h>
#include <zmq.h>
#include <unistd.h>
#include <getopt.h>
#include <stdlib.h>
#include <mpi.h>
#include <string.h>
#include <sys/time.h>
#include <assert.h>

#define ONE_SECOND           1000

typedef struct r{
    int value;
    struct r* next;
} rank_t;

typedef struct s{
    int value;
    rank_t * list_ranks;
    struct s* next;
} step_t;


#endif

