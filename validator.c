#include "validator.h"

struct uint256_t {
    uint32_t content[8];
} typedef uint256_t;

int to_wait_mpi = -1;
int MPIPORT  = 12345;
int SPARPORT = 23456;
step_t* first_step = NULL;
int        max_step = -1;
uint256_t* rank_buff;

uint256_t* global_mask;

uint256_t* init_256t(){
    uint256_t *place = (uint256_t*) malloc(sizeof(uint256_t));
    memset(place->content, 0, 8*sizeof(uint32_t));
    return place;
}

void init_masks(uint256_t* global_mask, uint32_t max_bit){
    uint32_t i = max_bit/(uint32_t)32;
    uint32_t shift = (max_bit % (uint32_t)32);
    if (i < 7){
        for(int j = i; j <8; j ++){
            global_mask->content[j] = ~global_mask->content[j];
        }
    }
    global_mask->content[i] = global_mask->content[i] << shift;
}

void set_bit(uint256_t* place, uint32_t bit){
    uint32_t i = bit/(uint32_t)32;
    place->content[i] = place->content[i] | ((uint32_t)1 << (bit % (uint32_t)32));
}

bool is_full(uint256_t* place){
    uint32_t rev = 0;
    return (
            (place->content[0]|global_mask->content[0]) &
            (place->content[1]|global_mask->content[1]) &
            (place->content[2]|global_mask->content[2]) &
            (place->content[3]|global_mask->content[3]) &
            (place->content[4]|global_mask->content[4]) &
            (place->content[5]|global_mask->content[5]) &
            (place->content[6]|global_mask->content[6]) &
            (place->content[7]|global_mask->content[7])
            ) == ~rev;
}

uint32_t get_count(uint32_t content, int offset, int max){
    uint32_t mask = 1;
    uint32_t count = 0;
    int pos = 1 + offset;
    for (int i = 0; i<32; i++){
        if (pos <= max){
            count += ((content >> i) & mask);
        }
        pos++;
    }
    return count;
}

float get_percentage(uint256_t* place, int max){
    uint32_t count =
        get_count(place->content[0]|global_mask->content[0], 0*32, max) +
            get_count(place->content[1]|global_mask->content[1], 1*32, max) +
            get_count(place->content[2]|global_mask->content[2], 2*32, max) +
            get_count(place->content[3]|global_mask->content[3], 3*32, max) +
            get_count(place->content[4]|global_mask->content[4], 4*32, max) +
            get_count(place->content[5]|global_mask->content[5], 5*32, max) +
            get_count(place->content[6]|global_mask->content[6], 6*32, max) +
            get_count(place->content[7]|global_mask->content[7], 7*32, max);
    return (float) ((float)count * (float)100) / (float)max;
}

long long current_timestamp() {
    struct timeval te; 
    gettimeofday(&te, NULL); // get current time
    long long milliseconds = te.tv_sec*1000LL + te.tv_usec/1000; // calculate milliseconds
    // printf("milliseconds: %lld\n", milliseconds);
    return milliseconds;
}

char** str_split(char* a_str, const char a_delim){
    char** result    = 0;
    size_t count     = 0;
    char* tmp        = a_str;
    char* last_comma = 0;
    char delim[2];
    delim[0] = a_delim;
    delim[1] = 0;

    /* Count how many elements will be extracted. */
    while (*tmp)
    {
        if (a_delim == *tmp)
        {
            count++;
            last_comma = tmp;
        }
        tmp++;
    }

    /* Add space for trailing token. */
    count += last_comma < (a_str + strlen(a_str) - 1);

    /* Add space for terminating null string so caller
       knows where the list of returned strings ends. */
    count++;

    result = malloc(sizeof(char*) * count);

    if (result)
    {
        size_t idx  = 0;
        char* token = strtok(a_str, delim);

        while (token)
        {
            assert(idx < count);
            *(result + idx++) = strdup(token);
            token = strtok(0, delim);
        }
        assert(idx == count - 1);
        *(result + idx) = 0;
    }

    return result;
}

long long curent_timestamp() {
    struct timeval te;
    gettimeofday(&te, NULL); // get curent time
    long long milliseconds = te.tv_sec*1000LL + te.tv_usec/1000; // calculate milliseconds
    // printf("milliseconds: %lld\n", milliseconds);
    return milliseconds;
}

void my_free (void *data, void *hint){
   free (data);
}

/*
 * Retrieve a message from the producer
 */
zmq_msg_t* get_message(void* socket){
    // receive the message
    zmq_msg_t* msg = malloc(sizeof(zmq_msg_t));
    assert(zmq_msg_init(msg) == 0);
    if(zmq_msg_recv(msg, socket, ZMQ_NOBLOCK) == -1){
        assert(errno == EAGAIN);
        free(msg);
    }else{
        return msg;
    }
    return 0;
}

void do_validation(erebor* e, char* username, int duration){

    // ini csv files
    char* output_csv = malloc(100);
    char* output_csv2 = malloc(100);
    sprintf(output_csv, "/home/%s/Documents/these/missing.csv", username);
    sprintf(output_csv2, "/home/%s/Documents/these/th_steps.csv", username);
    FILE* outcsv = fopen(output_csv, "w");
    fprintf(outcsv, "step,is_full\n");
    FILE* outcsv2 = fopen(output_csv2, "w");
    fprintf(outcsv2, "second,nb_steps\n");

    //compose the endpoints
    char* spark_endpoint = malloc(400);
    char* mpi_endpoint   = malloc(400);
    sprintf(spark_endpoint, "tcp://*:%d", SPARPORT);
    sprintf(mpi_endpoint,   "tcp://*:%d", MPIPORT);

    // initialise zmq
    void * context = zmq_ctx_new();
    zmq_ctx_set(context, ZMQ_IO_THREADS, 2);
    void * mpi_socket   = zmq_socket(context, ZMQ_PULL); // producer
    void * spark_socket = zmq_socket(context, ZMQ_PULL); // producer
    if (zmq_bind (mpi_socket, mpi_endpoint) == -1){
        erebor_print_error();
        printf("failure binding mpi socket %s", mpi_endpoint);
        fflush(stdout);
        exit(-1);
    }
    if (zmq_bind (spark_socket, spark_endpoint) == -1){
        erebor_print_error();
        printf("failure binding mpi socket %s", spark_endpoint);
        fflush(stdout);
        exit(-1);
    }

    // Time tracking to work only for Duration
    int keepup = 1;
    long long start_time     = curent_timestamp();
    long long second_tracker = start_time;
    float second=0;
    int count = 0;
    int last_second_received = 0;
    /**************************************************************************
     *
     *                          Main Event Loop
     *
     *************************************************************************/
    while(keepup){
        zmq_msg_t* message = get_message(mpi_socket);
        if(message > 0){
            int* data = (int*)zmq_msg_data(message);
            max_step = data[1];
            zmq_msg_close(message);
            free(message);
        }
        message = get_message(spark_socket);
        if(message > 0){
            int* data = (int*)zmq_msg_data(message);
            int rank = data[0];
            int step = data[1];
            set_bit(&rank_buff[step], rank);
            zmq_msg_close(message);
            free(message);
            last_second_received++;
        }
        long long now = current_timestamp();
        keepup = now - start_time < duration;
        if(count % 100 == 0){
            long long now = current_timestamp();
            keepup = now - start_time < duration;
            // Every seconds write CSV
            if(!keepup || now - second_tracker > ONE_SECOND){
                second += ((now -second_tracker) / ONE_SECOND);
                second_tracker = now;
                fprintf(outcsv2, "%f,%d\n", second, last_second_received);
                last_second_received = 0;
            }
        }
        count++;
    }
    // write missings
    for(int i=0; i<max_step; i++){
        fprintf(outcsv, "%d,%f\n", i, get_percentage(&rank_buff[i], to_wait_mpi));
    }
    fflush(outcsv2);
    fclose(outcsv2);
    fflush(outcsv);
    fclose(outcsv);
}

int main(int argc, char** argv){
    //Parse options
    int opt;
    char* in_ipc;
    char* out_ipc = malloc(100);
    char* network;
    char* username;
    int duration, nb_sending_sockets;
    do{
        opt = getopt (argc, argv, "P:N:U:H:D:");
        switch (opt) {
            case 'P':
                in_ipc = optarg;
                int iout_ipc = atoi(optarg) + 1300;
                sprintf(out_ipc, "%d", iout_ipc);
                break;
            case 'N':
                network = optarg;
                break;
            case 'U':
                username = optarg;
                break;
            case 'H':
                nb_sending_sockets = atoi(optarg);
                break;
            case 'D':
                duration = atoi(optarg);
                break;
        }
    } while (opt != -1);
    // Variables to handle answers
    char *dest    = malloc(100);
    char *group   = malloc(100);

    rank_buff = malloc(  10000000*sizeof(uint256_t));
    memset(rank_buff, 0, 10000000*sizeof(uint256_t));

    // Connect to Yggdrasil
    char* str_rank = "0";
    erebor* e = malloc(sizeof(erebor));
    int ret = erebor_init_connection(str_rank, network, in_ipc, out_ipc, e);

    if(ret == -1){
        erebor_print_error();
    }else{
        int keepup = 1;
        int keep_wait = 1;
        long long start_time = curent_timestamp();

        //compose the endpoints
        char* spark_endpoint = malloc(400);
        char* mpi_endpoint   = malloc(400);
        char* hostname = malloc(200);
        gethostname(hostname, 299);
        sprintf(spark_endpoint, "tcp://%s:%d", hostname, SPARPORT);
        sprintf(mpi_endpoint,   "tcp://%s:%d", hostname, MPIPORT);

        // variable to wait for everyone before startup
        int spark_contact = 0;
        to_wait_mpi   = -1;
        int mpi_contact   = 0;
        //Receive all informations to start
        while(keepup && keep_wait){
            char message[1000];
            int ret = erebor_non_block_recv(e, dest, group, message);
            assert(ret != -1);
            if(ret == 0){
                if(strncmp(message, "spark", strlen("spark")) ==0){
                    printf("received a spark call\n");
                    //register spark
                    spark_contact++;
                    erebor_send_to(e, dest, group, spark_endpoint);
                }
                if(strncmp(message, "size", strlen("size")) ==0){
                    printf("received a mpi size --%s--\n", message);
                    fflush(stdout);
                    //get the number of MPI rank
                    to_wait_mpi = atoi(str_split(message, ':')[1]);
                    global_mask = init_256t();
                    init_masks(global_mask, to_wait_mpi);
                }
                if(strncmp(message, "rank", strlen("rank")) ==0){
                    printf("received a mpi call\n");
                    //register the MPI rank
                    mpi_contact++;
                    erebor_send_to(e, dest, group, mpi_endpoint);
                }
            }
            keep_wait = !(to_wait_mpi > -1 && to_wait_mpi == mpi_contact
                          && spark_contact == nb_sending_sockets);
            long long now = curent_timestamp();
            keepup = now - start_time < duration;
        }
        if(keep_wait != 0){
            printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
            printf("!!                                                                            !!\n");
            printf("!!                                                                            !!\n");
            printf("!!                                                                            !!\n");
            printf("!!                     Failed at first contact                                !!\n");
            printf("!!                                                                            !!\n");
            printf("!!                                                                            !!\n");
            printf("!!                                                                            !!\n");
            printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
        }else{
            // Everyone is connected.
            do_validation(e, username, duration);
        }
        // Close Yggdrasil connexion
        erebor_close(e);
    }
    free(dest);
    free(group);
    free(e);
    return 0;
}
